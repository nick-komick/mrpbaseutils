# mrpBaseUtils 1.1.2

* Add additional numeric_diff functions (percent and difference)

# mrpBaseUtils 1.1.1

* Fix bug in identifying added and removed columns

# mrpBaseUtils 1.1.0

* Removed `stringr` dependence to focus on using the `glue` package

# mrpBaseUtils 1.0.2

* Fix issue with numeric difference in comparing lists of data frames

# mrpBaseUtils 1.0.1

* Fix bug in types when comparing data frames
* Correct and update some function documentation


# mrpBaseUtils 1.0.0

* Ported code from mrpUtilities package (stripped out unnecessary db connection details)

