//Code was modified from: https://gist.github.com/mlocati/21a9233ac83f7d3d7837535bc109b3b7


#include <Rcpp.h>
using namespace Rcpp;

// Undefine the Realloc macro, which is defined by both R and by Windows stuff
#undef Realloc
// Also need to undefine the Free macro
#undef Free

#if (OS_WINDOWS)

#include <windows.h>
#include <conio.h>

typedef NTSTATUS (WINAPI*RtlGetVersionPtr)(PRTL_OSVERSIONINFOW);
#endif



#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#endif


//' Enable colour on window console
// [[Rcpp::export(name = ".enable_win_console_colour")]]
IntegerVector enable_win_console_colour()
{


#if (OS_WINDOWS)
  BOOL result = FALSE;
  HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
  hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
  if (hStdOut != INVALID_HANDLE_VALUE) {
    DWORD mode;
    if (GetConsoleMode(hStdOut, &mode)) {
      if (!(mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING)) {
        mode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
        if (SetConsoleMode(hStdOut, mode)) {
          result = TRUE;
        }
      }
    }
  }

  return result;
#else
  return 0;
#endif

}
