#' mrpBaseUtils
#'
#' This package incorporates general utility functions used by other MRP R packages
#'
#' @docType package
#' @author Michael O'Brien, Nicholas Komick
#' @importFrom Rcpp sourceCpp
#' @useDynLib mrpBaseUtils, .registration = TRUE
#' @name mrpBaseUtils
NULL
