qwantz <- NULL
qwantz_debug_mode <- NULL

#' Logs warnings for MRP core
#'
#' @param ... character string of the message, use curly brackets (braces) to input objects (glue notation)
#'
#' @return Warning message. Writes to console (in yellow) and to .log file
#'
#' @examples
#' \dontrun{
#' logMrpWarn("the date is {Sys.Date()}")
#' }
#'
#' @importFrom glue glue
#' @importFrom cli cat_line
#'
#' @export
#'
logMrpWarn <- function(...) {
  logMsg <- glue("WARN", "[{Sys.time()}]", ..., .sep = " ", .envir = parent.frame())
  printMsg <- glue("WARN - ", ..., sep = " ", .envir = parent.frame())

  cat_line(printMsg, col = "yellow", background_col = "white")

  if (exists("qwantz")) {
    if (!is.null(qwantz)) {
      write(x = glue("\r\n", logMsg), file = qwantz, append = TRUE)
    }
  }
}

#' Logs errors and stops functions for MRP core
#'
#' @param ... character string of the message, use curly brackets (braces) to input objects (glue notation)
#'
#' @return Error message. Writes to console (in red) and to .log file.  Stops a function
#'
#' @examples
#' \dontrun{
#' logMrpError("the date is {Sys.Date()}")
#' }
#'
#' @importFrom glue glue
#' @importFrom cli cat_line
#'
#' @export
#'
logMrpError <- function(...) {
  logMsg <- glue("ERROR [{Sys.time()}]", ..., .sep = " ", .envir = parent.frame())
  printMsg <- glue("ERROR", ..., .sep = " ", .envir = parent.frame())

  if (exists("qwantz")) {
    if (!is.null(qwantz)) {
      write(x = paste0("\r\n", logMsg), file = qwantz, append = TRUE)
    }
  }
  cat_line(printMsg, col = "red", background_col = "white")
  return(printMsg)
}


#' Logs info for MRP core
#'
#' @param ... character string of the message, use curly brackets (braces) to input objects (glue notation)
#'
#' @return Warning message. Writes to console (in yellow) and to .log file
#'
#' @examples
#' \dontrun{
#' logMrpInfo("the date is {Sys.Date()}")
#' }
#'
#' @importFrom glue glue
#' @importFrom cli cat_line
#'
#' @export
#'
logMrpInfo <- function(...) {
  logMsg <- glue("INFO", "[{Sys.time()}]-", ..., .sep = "", .envir = parent.frame())
  printMsg <- glue("INFO-", ..., .sep = "", .envir = parent.frame())

  cat_line(printMsg)

  if (exists("qwantz")) {
    if (!is.null(qwantz)) {
      write(x = paste0("\r\n", logMsg), file = qwantz, append = TRUE)
    }
  }
}

#' Logs debug messages for MRP core if debug_mode = TRUE in the connectMrpLog function
#'
#' @param ... character string of the message, use curly brackets (braces) to input objects (glue notation)
#'
#' @return Warning message. Writes to console (in yellow) and to .log file
#'
#' @examples
#' \dontrun{
#' logMrpDebug("the date is {Sys.Date()}")
#' }
#'
#' @importFrom glue glue
#' @importFrom cli cat_line
#'
#' @export
#'
logMrpDebug <- function(...) {
  if (qwantz_debug_mode) {
    logMsg <- glue("DEBUG [{Sys.time()}]-", ..., .sep = " ", .envir = parent.frame())
    printMsg <- glue("DEBUG -", ..., .sep = " ", .envir = parent.frame())

    cat_line(printMsg)

    if (exists("qwantz")) {
      write(x = paste0("\r\n", logMsg), file = qwantz, append = TRUE)
    }
  }
}


#' Sets up file connection (named 'qwantz' to avoid user overwrite) to be used with mrpLogWarn etc.
#'
#' @param log_file the name in quotes of the .log file you want to write to.
#' @param debug_mode Should the debug messages written?
#'
#' @export
connectMrpLog <- function(log_file = "mrpLog.log", debug_mode = FALSE) {
  qwantz <<- file(log_file, open = "ab")
  qwantz_debug_mode <<- debug_mode
}

#' Set the logging to enable debug messages
#'
#' @param debug_mode Boolean flag if debug messages should be written to the log and console
#'
#' @export
setMrpLogDebug <- function(debug_mode) {
  qwantz_debug_mode <<- debug_mode
}



#' Enable the colour on the windows console
#'
#' @export
enableWinConsoleColour <- function() {
  .enable_win_console_colour()
  Sys.setenv(ConEmuANSI = "ON")
}
